import java.io.File;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class ThreeDigits {
	private static Scanner file;
	
	public static void main(String[] args) {
		openFile(args[1]);
		
		String startNode = readLine();
		String goalNode = readLine();
		int[] forbiddenList = readForbidden();
		
		/*System.out.println(startNode);
		System.out.println(goalNode);
		if(forbiddenList!=null) {
			for(int i=0; i<forbiddenList.length; i++) {
				System.out.print(forbiddenList[i] + ",");
			}
		}	
		System.out.println();*/
		
		switch(args[0]) {
			case "B":
				//do BFS search
				//System.out.println("BFS Search activated");
				BFS puzzleBFS = new BFS(startNode, goalNode, forbiddenList);
				puzzleBFS.runBFS();
				break;
			case "D":
				//do DFS search
				DFS puzzleDFS = new DFS(startNode, goalNode, forbiddenList);
				puzzleDFS.runDFS();
				//System.out.println("DFS Search activated");
				break;
			case "I":
				//do IDS search
				System.out.println("IDS Search activated");
				break;
			case "G":
				//do Greedy Search
				System.out.println("Greedy Search activated");
				break;
			case "A":
				//do A* search
				System.out.println("A* Search activated");
				break;
			case "H":
				//do Hill-climbing search
				System.out.println("Hill-Climbing Search activated");
				break;
			default:
				System.out.println("Unknown search");
				System.exit(0);
		}
		
	}
	
	public static void openFile(String fileName) {
		try {
			file = new Scanner(new File(fileName));
		}
		catch(Exception e) {
			System.out.println("Error, File not found");
			System.exit(0);
		}
		
	}
	
	public static String readLine() {
		String line;
		if(file.hasNext()) {
			line = file.next();
			return line;
		}
		else {
			return null;
		}
	}
	
	public static int[] readForbidden() {
		String[] forbiddenList;
		int[] forbiddenInts = null;
		String line;
		if(file.hasNext()) {
			line = file.next();
			forbiddenList = line.split(",");
			forbiddenInts = new int[forbiddenList.length];
			for(int i=0; i<forbiddenList.length; i++) {
				forbiddenInts[i] = Integer.parseInt(forbiddenList[i]);
			}
			Arrays.sort(forbiddenInts);
			return forbiddenInts;
		}
		else {
			return null;
		}
	}
	
}
