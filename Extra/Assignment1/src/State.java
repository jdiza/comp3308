import java.util.ArrayList;

public class State {
	private String strValue;
	private int intValue;
	private boolean goal;
	ArrayList<State> children;
	
	public State(String value) {
		this.strValue = value;
		this.intValue = Integer.parseInt(value);
		this.goal = false;
	}
	
	public String getStrValue() {
		return this.strValue;
	}
	public int getIntValue() {
		return this.intValue;
	}
	public ArrayList<State> getChildren() {
		return this.children;
	}
	public boolean isGoal() {
		return this.goal;
	}
	public void setGoal(boolean goalValue) {
		this.goal = goalValue;
	}
	
	public void generateChildren() {
		if(this.intValue >= 100) {
			State childA = new State(Integer.toString(this.intValue - 100));
			this.children.add(childA);
		}
		
		if(this.intValue < 900) {
			State childB = new State(Integer.toString(this.intValue + 100));
		}
		
		State childC = new State(Integer.toString(this.intValue - 10));
		State childD = new State(Integer.toString(this.intValue + 10));
		State childE = new State(Integer.toString(this.intValue - 1));
		State childF = new State(Integer.toString(this.intValue + 1));
	}
}
