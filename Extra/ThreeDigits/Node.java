import java.util.ArrayList;
import java.util.List;

public class Node {
	private int value;
	private int digit0;
	private int digit1;
	private int digit2;
	private List<Node> children;
	private Node parent;
	private String stateType;
	
	public Node(int value, int digit0, int digit1, int digit2) {
		this.value = value;
		this.digit0 = digit0;
		this.digit1 = digit1;
		this.digit2 = digit2;
		this.children = new ArrayList<Node>();
		this.stateType = "unknown";
	}
	
	public Node getParent() {
		return this.parent;
	}
	
	public void setParent(Node givenParent) {
		this.parent = givenParent;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public int getDigit(int digitLocation) {
		switch(digitLocation) {
		case 0: 
			return this.digit0;
		case 1: 
			return this.digit1;
		case 2: 
			return this.digit2;
		default: 
			System.out.println("Digit not found.");
			return 999;
		}
	}
	
	public void setDigit(int digitLocation, int digitValue) {
		if((digitValue >=10) || (digitValue <0)) {
			System.out.println("Illegal digit value");
		}
		switch(digitLocation) {
		case 0: 
			this.digit0 = digitValue;
		case 1: 
			this.digit1 = digitValue;
		case 2: 
			this.digit2 = digitValue;
		default: 
			System.out.println("Digit not found.");
		}
	}
	
	public List<Node> getChildren() {
		return this.children;
	}
	
	public Node createChildA() {
		Node newNode;
		if(this.digit0 == 0) {
			return null;
		}
		else {
			newNode = new Node(this.value - 100, this.digit0-1, this.digit1, this.digit2);
			newNode.setStateType("A");
		}
		return newNode;
	}
	
	public Node createChildB() {
		Node newNode;
		if(this.digit0 == 9) {
			return null;
		}
		else {
			newNode = new Node(this.value + 100, this.digit0+1, this.digit1, this.digit2);
			newNode.setStateType("B");
		}
		return newNode;
	}
	
	public Node createChildC() {
		Node newNode;
		if(this.digit1 == 0) {
			return null;
		}
		else {
			newNode = new Node(this.value - 10, this.digit0, this.digit1-1, this.digit2);
			newNode.setStateType("C");
		}
		return newNode;
	}
	
	public Node createChildD() {
		Node newNode;
		if(this.digit1 == 9) {
			return null;
		}
		else {
			newNode = new Node(this.value + 10, this.digit0, this.digit1+1, this.digit2);
			newNode.setStateType("D");
		}
		return newNode;
	}
	
	public Node createChildE() {
		Node newNode;
		if(this.digit2 == 0) {
			return null;
		}
		else {
			newNode = new Node(this.value - 1, this.digit0, this.digit1, this.digit2-1);
			newNode.setStateType("E");
		}
		return newNode;
	}
	
	public Node createChildF() {
		Node newNode;
		if(this.digit2 == 9) {
			return null;
		}
		else {
			newNode = new Node(this.value + 1, this.digit0, this.digit1, this.digit2+1);
			newNode.setStateType("F");
		}
		return newNode;
	}
	
	public void generateChildren() {
		Node nodeA = createChildA();
		Node nodeB = createChildB();
		Node nodeC = createChildC();
		Node nodeD = createChildD();
		Node nodeE = createChildE();
		Node nodeF = createChildF();
		List<Node> newChildren = new ArrayList<Node>();
		if((nodeA != null) && (!this.stateType.equals("A")) && (!this.stateType.equals("B"))) {
			nodeA.setParent(this);
			newChildren.add(nodeA);
		}
		if((nodeB != null) && (!this.stateType.equals("B")) && (!this.stateType.equals("A"))) {
			nodeB.setParent(this);
			newChildren.add(nodeB);
		}
		if((nodeC != null) && (!this.stateType.equals("C")) && (!this.stateType.equals("D"))){
			nodeC.setParent(this);
			newChildren.add(nodeC);
		}
		if((nodeD != null) && (!this.stateType.equals("D")) && (!this.stateType.equals("C"))){
			nodeD.setParent(this);
			newChildren.add(nodeD);
		}
		if((nodeE != null) && (!this.stateType.equals("E")) && (!this.stateType.equals("F"))){
			nodeE.setParent(this);
			newChildren.add(nodeE);
		}
		if((nodeF != null) && (!this.stateType.equals("F")) && (!this.stateType.equals("E"))){
			nodeF.setParent(this);
			newChildren.add(nodeF);
		}
		children = newChildren;
	}
	
	public void printState() {
		System.out.printf("%d%d%d", this.digit0, this.digit1, this.digit2);
	}
	
	public void setStateType(String s) {
		this.stateType = s;
	}
	
	public String getStateType() {
		return this.stateType;
	}
	
}
