import java.util.ArrayList;
import java.util.List;

public class State {
	private int intValue;
	private String strValue;
	private int digit0;
	private int digit1;
	private int digit2;
	private List<State> children;
	private State parent;
	private String stateType;
	
	public State(int value, int digit0, int digit1, int digit2) {
		this.intValue = value;
		this.strValue = Integer.toString(value);
		this.digit0 = digit0;
		this.digit1 = digit1;
		this.digit2 = digit2;
		this.children = new ArrayList<State>();
		this.stateType = "unknown";
	}
	
	public State getParent() {
		return this.parent;
	}
	public int getIntValue() {
		return this.intValue;
	}
	public String getStrValue() {
		return this.strValue;
	}
	public int getDigit(int digitLocation) {
		switch(digitLocation) {
		case 0: 
			return this.digit0;
		case 1: 
			return this.digit1;
		case 2: 
			return this.digit2;
		default: 
			System.out.println("Digit not found.");
			return 999;
		}
	}
	public List<State> getChildren() {
		return this.children;
	}
	public String getStateType() {
		return this.stateType;
	}
	public void setParent(State parent) {
		this.parent = parent;
	}
	public void setStateType(String stateType) {
		this.stateType = stateType;
	}
	public void printState() {
		System.out.printf(this.strValue);
	}
	public State createChildA() {
		State newState;
		if(this.digit0 == 0) {
			return null;
		}
		else {
			newState = new State(this.intValue - 100, this.digit0-1, this.digit1, this.digit2);
			newState.setStateType("A");
		}
		return newState;
	}
	
	public State createChildB() {
		State newState;
		if(this.digit0 == 9) {
			return null;
		}
		else {
			newState = new State(this.intValue + 100, this.digit0+1, this.digit1, this.digit2);
			newState.setStateType("B");
		}
		return newState;
	}
	
	public State createChildC() {
		State newState;
		if(this.digit1 == 0) {
			return null;
		}
		else {
			newState = new State(this.intValue - 10, this.digit0, this.digit1-1, this.digit2);
			newState.setStateType("C");
		}
		return newState;
	}
	
	public State createChildD() {
		State newState;
		if(this.digit1 == 9) {
			return null;
		}
		else {
			newState = new State(this.intValue + 10, this.digit0, this.digit1+1, this.digit2);
			newState.setStateType("D");
		}
		return newState;
	}
	
	public State createChildE() {
		State newState;
		if(this.digit2 == 0) {
			return null;
		}
		else {
			newState = new State(this.intValue - 1, this.digit0, this.digit1, this.digit2-1);
			newState.setStateType("E");
		}
		return newState;
	}
	
	public State createChildF() {
		State newState;
		if(this.digit2 == 9) {
			return null;
		}
		else {
			newState = new State(this.intValue + 1, this.digit0, this.digit1, this.digit2+1);
			newState.setStateType("F");
		}
		return newState;
	}

	public void generateChildren() {
		State stateA = createChildA();
		State stateB = createChildB();
		State stateC = createChildC();
		State stateD = createChildD();
		State stateE = createChildE();
		State stateF = createChildF();
		List<State> newChildren = new ArrayList<State>();
		if((stateA != null) && (!this.stateType.equals("A")) && (!this.stateType.equals("B"))) {
			stateA.setParent(this);
			newChildren.add(stateA);
		}
		if((stateB != null) && (!this.stateType.equals("B")) && (!this.stateType.equals("A"))) {
			stateB.setParent(this);
			newChildren.add(stateB);
		}
		if((stateC != null) && (!this.stateType.equals("C")) && (!this.stateType.equals("D"))){
			stateC.setParent(this);
			newChildren.add(stateC);
		}
		if((stateD != null) && (!this.stateType.equals("D")) && (!this.stateType.equals("C"))){
			stateD.setParent(this);
			newChildren.add(stateD);
		}
		if((stateE != null) && (!this.stateType.equals("E")) && (!this.stateType.equals("F"))){
			stateE.setParent(this);
			newChildren.add(stateE);
		}
		if((stateF != null) && (!this.stateType.equals("F")) && (!this.stateType.equals("E"))){
			stateF.setParent(this);
			newChildren.add(stateF);
		}
		children = newChildren;
	}
}
