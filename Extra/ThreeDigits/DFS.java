import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class DFS {
	private List<Node> states;
	private List<Node> expandedNodes;
	private List<Node> fringe;
	private List<Node> path;
	private int[] forbidden;
	
	private Node root;
	private Node goal;
	private Node currentNode;
	
	
	public DFS(String givenRoot, String givenGoal, int[] forbidden) {
		int root0 = Character.getNumericValue(givenRoot.charAt(0));
		int root1 = Character.getNumericValue(givenRoot.charAt(1));
		int root2 = Character.getNumericValue(givenRoot.charAt(2));
		
		int goal0 = Character.getNumericValue(givenGoal.charAt(0));
		int goal1 = Character.getNumericValue(givenGoal.charAt(1));
		int goal2 = Character.getNumericValue(givenGoal.charAt(2));
		this.root = new Node(Integer.parseInt(givenRoot), root0, root1, root2);
		this.goal = new Node (Integer.parseInt(givenGoal), goal0, goal1, goal2);
		this.forbidden = forbidden;
		this.expandedNodes = new ArrayList<Node>();
		this.currentNode = this.root;
		this.fringe = new ArrayList<Node>();
		this.path = new ArrayList<Node>();
		
		this.fringe.add(this.root);
		//this.path.add(this.root);
	}
	
	public void expand(Node givenNode) {
		//givenNode.generateChildren();
		fringe.addAll(givenNode.getChildren());
		fringe.addAll(0, givenNode.getChildren());
	}
	
	public void runDFS() {
		int counter = 0;
		currentNode = fringe.remove(counter);
		currentNode.generateChildren();
		expand(currentNode);
		expandedNodes.add(currentNode);
		int currentValue = currentNode.getValue();
		int stateCount = 1;
		while(checkGoalNode(currentNode) != true) {
			if(counter!=fringe.size()) {
				currentNode = fringe.remove(counter);
				currentValue = currentNode.getValue();
				
			}
			if(this.forbidden!=null) {
				if(checkForbidden(currentNode) == true) {//if node matches forbidden node
					continue;
				}
			}

			if(currentNode != this.root) {
				if(isCycle(currentNode) == true) {//if currently in a cycle
					continue;
				}
			}
			expand(currentNode);
			expandedNodes.add(currentNode);
			stateCount += 1;
			if(expandedNodes.size() > 1000) {
				System.out.println("No solution found.");
				printExpanded();
				return;
			}
		}
	}
	
	public boolean checkGoalNode(Node givenNode) {
		if(givenNode.getValue() == goal.getValue()){
			//System.out.println("Solution Path");
			expandedNodes.add(givenNode);
			getPath(givenNode);
			printPath();
			System.out.println();
			//System.out.println("Expanded Nodes");
			printExpanded();	
			System.exit(0);
			return true;

		}
		else {
			return false;
		}
	}
	
	public void printExpanded() {
		//System.out.println("Expanded Nodes");
		for(int i=0; i<expandedNodes.size()-1; i++) {
			expandedNodes.get(i).printState();
			if(i!=expandedNodes.size()-2) {
				System.out.print(" ");
			}
		}
		System.exit(0);
	}
	public boolean checkForbidden(Node givenNode) {
		if(Arrays.binarySearch(forbidden, givenNode.getValue())>=0) {//given Node is forbidden
			return true;
		}
		else {
			return false;
		}
	}
	
	public List<Node> getPath(Node givenNode) {
		Node current = givenNode;
		path.add(current);
		current = givenNode.getParent();
		while(current!= this.root) {	
			if(current != null) {
				path.add(current);
			}
			current = current.getParent();
		}
		path.add(current);
		return path;
	}
	
	public void printPath() {
		Node current;
		int counter = path.size()-1;
		while(counter!=-1) {
			current = path.get(counter);
			current.printState();
			if(counter != 0) {
				System.out.print(" ");
			}
			counter-=1;
		}
	}
	
	public boolean isCycle(Node givenNode) {//if true, node exists already
		boolean overallStatus = false;
		boolean isNodeSame;
		givenNode.generateChildren();
		int givenNodeValue = givenNode.getValue();
		for(Node s: expandedNodes) {	
			int exploredNodeValue = s.getValue();
			if(givenNode.getValue() == s.getValue()) {
				if(givenNode.getChildren().size() == s.getChildren().size()) {
					overallStatus = true;
					for(int i=0; i<givenNode.getChildren().size(); i++) {
						if(givenNode.getChildren().get(i).getValue() != s.getChildren().get(i).getValue()) {
							overallStatus = false;
						}
					}
					if(overallStatus == true) {
						return true;
					}
				}
			}
		}
		return overallStatus;
	}
}
