import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class BFS {
	private List<State> states;
	private List<State> expandedStates;
	private Queue<State> fringe;
	private List<State> path;
	private int[] forbidden;
	
	private State root;
	private State goal;
	private State currentState;
	
	
	public BFS(String givenRoot, String givenGoal, int[] forbidden) {
		int root0 = Character.getNumericValue(givenRoot.charAt(0));
		int root1 = Character.getNumericValue(givenRoot.charAt(1));
		int root2 = Character.getNumericValue(givenRoot.charAt(2));
		
		int goal0 = Character.getNumericValue(givenGoal.charAt(0));
		int goal1 = Character.getNumericValue(givenGoal.charAt(1));
		int goal2 = Character.getNumericValue(givenGoal.charAt(2));
		this.root = new State(Integer.parseInt(givenRoot), root0, root1, root2);
		this.goal = new State (Integer.parseInt(givenGoal), goal0, goal1, goal2);
		this.forbidden = forbidden;
		this.expandedStates = new ArrayList<State>();
		this.currentState = this.root;
		this.fringe = new ArrayDeque<State>();
		this.path = new ArrayList<State>();
		
		this.fringe.add(this.root);
		//this.path.add(this.root);
	}
	
	public void expand(State givenState) {
		//givenState.generateChildren();
		fringe.addAll(givenState.getChildren());
	}
	
	public void runBFS() {
		currentState = fringe.remove();
		currentState.generateChildren();
		expand(currentState);
		expandedStates.add(currentState);
		int currentValue = currentState.getIntValue();
		int stateCount = 1;
		while(checkGoalState(currentState) != true) {
			if(fringe.isEmpty()==false) {
				currentState = fringe.remove();
				currentValue = currentState.getIntValue();
				
			}
			if(this.forbidden!=null) {
				if(checkForbidden(currentState) == true) {//if state matches forbidden state
					continue;
				}
			}

			if(currentState != this.root) {
				if(isCycle(currentState) == true) {//if currently in a cycle
					continue;
				}
			}
			expand(currentState);
			expandedStates.add(currentState);
			stateCount += 1;
			if(expandedStates.size() > 1000) {
				System.out.println("No solution found.");
				printExpanded();
				return;
			}
		}
	}
	
	public boolean checkGoalState(State givenState) {
		if(givenState.getIntValue() == goal.getIntValue()){
			//System.out.println("Solution Path");
			expandedStates.add(givenState);
			getPath(givenState);
			printPath();
			System.out.println();
			//System.out.println("Expanded States");
			printExpanded();	
			System.exit(0);
			return true;

		}
		else {
			return false;
		}
	}
	
	public void printExpanded() {
		//System.out.println("Expanded States");
		for(int i=0; i<expandedStates.size()-1; i++) {
			expandedStates.get(i).printState();
			if(i!=expandedStates.size()-2) {
				System.out.print(" ");
			}
		}
		System.exit(0);
	}
	public boolean checkForbidden(State givenState) {
		if(Arrays.binarySearch(forbidden, givenState.getIntValue())>=0) {//given State is forbidden
			return true;
		}
		else {
			return false;
		}
	}
	
	public List<State> getPath(State givenState) {
		State current = givenState;
		path.add(current);
		current = givenState.getParent();
		while(current != this.root) {	
			if(current != null) {
				path.add(current);
			}
			current = current.getParent();
		}
		path.add(current);
		return path;
	}
	
	public void printPath() {
		State current;
		int counter = path.size()-1;
		while(counter!=-1) {
			current = path.get(counter);
			current.printState();
			if(counter != 0) {
				System.out.print(" ");
			}
			counter-=1;
		}
	}
	
	public boolean isCycle(State givenState) {//if true, state exists already
		boolean overallStatus = false;
		boolean isStateSame;
		givenState.generateChildren();
		int givenStateValue = givenState.getIntValue();
		for(State s: expandedStates) {	
			int exploredStateValue = s.getIntValue();
			if(givenState.getIntValue() == s.getIntValue()) {
				if(givenState.getChildren().size() == s.getChildren().size()) {
					overallStatus = true;
					for(int i=0; i<givenState.getChildren().size()-1; i++) {
						if(givenState.getChildren().get(i).getIntValue() != s.getChildren().get(i).getIntValue()) {
							overallStatus = false;
						}
					}
					if(overallStatus == true) {
						return true;
					}
				}
			}
		}
		return overallStatus;
	}
}
