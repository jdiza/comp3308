
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ThreeDigits {

	public static void main(String[] args) {
		String algorithmUsed = args[0];
		File  file = new File(args[1]);
		ArrayList<State> configStates = readFile(file);
		for(State thisState : configStates) {
			System.out.println(thisState.getStrValue());
		}
	}
	
	public static ArrayList<State> readFile(File file) {
		ArrayList<State> configStates = new ArrayList<State>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			configStates.add(new State(br.readLine()));
			configStates.add(new State(br.readLine()));
			configStates.get(1).setGoal(true);
			String[] forbiddenStates = br.readLine().split(",");
			for(String state : forbiddenStates) {
				configStates.add(new State(state));
			}
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			try {
				br.close();
			}
			catch (IOException ioe) {
				System.out.println("Buffered Reader closing error");
			}
		}
		return configStates;
	}

}
